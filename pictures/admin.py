# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Picture

# Register your models here.
class PictureAdmin(admin.ModelAdmin):
    pass
admin.site.register(Picture, PictureAdmin)
