# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.

from .forms import PictureEditForm
from items.models import Item


@login_required
def new_picture(request, item_id):
    if request.method == 'GET':
        edit_form = PictureEditForm()
    elif request.method == 'POST':
        edit_form = PictureEditForm(request.POST, request.FILES)

        item = Item.objects.get(pk=item_id)

        # can only add images to items you own
        if edit_form.is_valid() and request.user.pk == item.user.pk:
            new_picture = edit_form.save(commit=False)
            new_picture.user = request.user
            new_picture.item = item
            new_picture.save()

            return redirect(item.get_absolute_url())

    return render(
        request,
        'new_picture.html',
        {
            'form': edit_form,
            'item': item_id,
        }
    )
