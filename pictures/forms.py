# coding: utf-8

from __future__ import unicode_literals
from django import forms
from .models import Picture


class PictureEditForm(forms.ModelForm):
    class Meta:
        model = Picture
        exclude = ['user', 'item']

