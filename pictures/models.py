# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse_lazy
from django.db import models
from django.conf import settings
from items.models import Item


# Create your models here.
class Picture(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    image_file = models.ImageField(upload_to='%Y/%m/%d')
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    item = models.ForeignKey(Item)  # every image is associated with an item

    def delete(self, *args, **kwargs):
        self.image_file.delete()
        self.filtered_image.delete()
        super(Picture, self).delete(*args, **kwargs)

    def get_absolute_url(self):
        return reverse_lazy('view_single_picture', kwargs={'picture_id': self.id})
