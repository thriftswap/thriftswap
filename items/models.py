# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse_lazy
from django.db import models
from django.conf import settings


# Create your models here.
class Item(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=500)

    # items are linked to items their users would like to swap for
    likes = models.ManyToManyField('Item', blank=True, related_name='liked_by')

    def get_absolute_url(self):
        return reverse_lazy('view_single_item', kwargs={'item_id': self.pk})
