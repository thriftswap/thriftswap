# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Item

# Register your models here.
class ItemAdmin(admin.ModelAdmin):
    list_display = ('name',)
    filter_horizontal = ('likes',)
admin.site.register(Item, ItemAdmin)

