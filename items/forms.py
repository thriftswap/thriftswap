# coding: utf-8

from __future__ import unicode_literals
from django import forms
from .models import Item


class ItemEditForm(forms.ModelForm):
    class Meta:
        model = Item
        exclude = ['likes', 'user']
