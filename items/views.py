# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.

from .models import Item
from .forms import ItemEditForm
from django.db.models.aggregates import Count
from random import randint
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist


def match_view(request, my_item, like_item):
    context = {'my_item': Item.objects.get(pk=my_item), 'like_item': Item.objects.get(pk=like_item)}
    return render(request, 'match.html', context)


def swap_view(request, item_id, like_item_id=None):

    if like_item_id is not None:
        my_item = Item.objects.get(pk=item_id)
        like_item = Item.objects.get(pk=like_item_id)
        like_item.likes.add(my_item)

        try:
            like_item.liked_by.get(pk=item_id)
            return redirect(reverse('match_view', kwargs={'my_item': my_item.pk, 'like_item': like_item.pk}))
        except ObjectDoesNotExist:
            pass

    size = Item.objects.aggregate(count=Count('id'))['count']
    rand_ind = randint(0, size-1)
    other_item = Item.objects.all()[rand_ind]

    context = {'other_item': other_item,
               'my_item': Item.objects.get(pk=item_id),
               'pictures': other_item.picture_set.all(),
               }
    return render(request, 'swap_item.html', context)


def single_item(request, item_id):
    item = Item.objects.get(pk=item_id)
    # response_text = '<p>{item_id}번 사진</p>'
    # response_text += '<img src="{item_url}">'
    context = {'item': item,
               'pictures': item.picture_set.all(),
               }

    return render(request, 'single_item.html', context)

    # return HttpResponse(response_text.format(
    #     item_id=item_id,
    #     item_url=item.image_file.url))


@login_required
def new_item(request):
    if request.method == 'GET':
        edit_form = ItemEditForm()
    elif request.method == 'POST':
        edit_form = ItemEditForm(request.POST, request.FILES)

        if edit_form.is_valid():
            new_item = edit_form.save(commit=False)
            new_item.user = request.user
            new_item.save()

            return redirect(new_item.get_absolute_url())

    return render(
        request,
        'new_item.html',
        {
            'form': edit_form,
        }
    )

