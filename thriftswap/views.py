from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate, get_user_model
from django.urls import reverse


def index_view(request):
    context = {}
    return render(request, 'index.html', context)


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def profile_view(request, profile_id=None):
    if profile_id is None:
        if request.user.is_authenticated():
            profile_id = request.user.pk
        else:
            return redirect(reverse('home'))

    items = get_user_model().objects.get(pk=profile_id).item_set.all()
    context = {'items': items}
    if request.user.is_authenticated:
        context['user'] = request.user
    return render(request, 'profile.html', context)

