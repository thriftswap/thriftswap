"""thriftswap URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from pictures import views as picture_views
from items import views as item_views
from django.conf import settings
import views

urlpatterns = [
    url(r'^$', views.index_view, name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^profile/(?P<profile_id>\d+)$', views.profile_view, name='profile_view'),
    url(r'^profile/$', views.profile_view, name='profile_view'),
    url(r'^item/(?P<item_id>\d+)/picture/$', picture_views.new_picture, name='new_picture'),
    url(r'^item/$', item_views.new_item, name='new_item'),
    url(r'^item/(?P<item_id>\d+)$', item_views.single_item, name='view_single_item'),
    url(r'^item/(?P<item_id>\d+)/swap$', item_views.swap_view, name='swap_item'),
    url(r'^item/(?P<item_id>\d+)/swap/(?P<like_item_id>\d+)$', item_views.swap_view, name='swap_item'),
    url(r'^match/(?P<my_item>\d+)/(?P<like_item>\d+)$', item_views.match_view, name='match_view'),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
